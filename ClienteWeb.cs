using System.Net.Http;
using System.Threading.Tasks;

namespace SerializationLession
{
    public class ClienteWeb
    {
        private readonly string url;
        public ClienteWeb(string url)
        {
            this.url = url;
        }

        public async Task<string> EnviarRequisicao()
        {
            var ClienteWeb = new HttpClient();
            var tarefa = ClienteWeb.GetStringAsync(this.url);
            return await tarefa;
            
        }
    }
}